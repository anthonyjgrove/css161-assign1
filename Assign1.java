/**
 * @author Anthony Grove
 * CSS 161 - Assignment 1
 * Lyrics to "I Want To Hold Your Hand"
 */
public class Assign1{
    public static void main(String args[]){
        first();
        System.out.println();
        second(); 
        System.out.println();
        third();
        System.out.println();
        fourth();
        System.out.println();
        third();
        System.out.println();
        fourth();
    }
    //print first chorus 
    public static void first() {
        String a = "I wanna hold your hand";
        System.out.println("I'll tell you something");
        System.out.println("I think you'll understand");
        System.out.println("When I say that something");
        hand();
    }
    //print third chorus
    public static void second() {
        System.out.println("Oh please, say to me");
        System.out.println("You'll let me be your man");
        System.out.println("And please, say to me");
        System.out.println("You'll let me hold your hand");
        System.out.println("Now, let me hold your hand");
        System.out.println("I wanna hold your hand");       
    }
    //print third chorus
    public static void third() {

        String b = "I can't hide";
        System.out.println("And when I touch you");
        System.out.println("I feel happy, inside");
        System.out.println("It's such a feeling that my love");
        System.out.println(b + "\n" + b + "\n" + b);
    }
    //print fourth chorus
    public static void fourth() {
        String a = "I wanna hold your hand";
        System.out.println("Yeah, you got that something");
        System.out.println("I think you'll understand");
        System.out.println("When I say that something");
        hand();  
    }
    //print for repeated phrase
    public static void hand() {
        String a = "I wanna hold your hand";
        System.out.println(a);
        System.out.println(a);
        System.out.println(a);
    }
    

}
